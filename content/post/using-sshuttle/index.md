---
title: sshuttle
description: Туннель через SSH, или три запрещённые буквы.
slug: using-sshuttle
date: 2025-02-18
tags:
    - ssh
    - linux
image: sshuttle.png
---

[SSHUTTLE][1]

Простой скрипт для установки и запуска туннеля ssh.

Использую на серверах linux для обхода территориальных ограничений, например обновление клиента Tailscale или установки/обновления Terraform.  

Для использования клонируем [репозиторий][1], выдаем права на запуск файла
```bash
chmod u+x ./vpn.sh
```
и выполняем команду
```bash
sudo ./vpn.sh sshuser=test-user sshserver=127.0.0.1
```
вписывая данные вашего сервера linux который находится на тех территория откуда есть доступ к нужным вам ресурсам.

Скрипт установит и/или запустит программу sshuttle.

После ввода пароля от сервера ssh должна появится строка:
```bash
client: Connected.
```
Готово, для проверки можно ввести команду
```bash
curl ifconfig.io
```
которая должна показать IP сервера ssh.


[1]: https://gitverse.ru/mashkov/using-sshuttle

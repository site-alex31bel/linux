---
title: Hello World
description: Привет, мир!
slug: hello-world
date: 2024-05-22
tags:
    - goHugo
image: gohugo.png
---

## Привет, мир!
Это моя первая запись в блоге, работающем на [hugo][1]

[1]: https://gohugo.io

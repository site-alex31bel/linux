---
title: Из sh в bash
description: Переключаем командую оболочку из sh в bash
slug: sh_in_bash
date: 2024-05-27
tags:
    - Linux
image: 1.jpg
---

> Столкнулся с небольшой проблемой в терминале при попытке посмотреть историю команд кнопкой вверх(вниз, лево и право) появляются непонятный набор символов

```bash
$ ^[[A^[[B^[[C^[[D
```
![Screenshot1](scr1.png)

Оказалось при создании виртуальных машин в cloud.ru во всех (скорее всего) дистрибутивах (я пробовал `Debian` и `CentOS`) по умолчанию используется командная оболочка `/bin/sh`.

Есть несколько способов определить какая командная оболочка работает на данный момент:

* Способ №1
```bash
$ echo $SHELL
/bin/bash
```
* Способ №2
```bash
$ echo $0
bash
```
* Способ №3
```bash
$ ps -p $$
PID TTY TIME CMD
10792 pts/3 00:00:00 bash
```

Для временного переключения на `/bin/bash` необходимо выполнить команду

```bash
$ /bin/bash
```
![Screenshot2](scr2.png)

Для изменения командной оболочки по умолчанию есть команда `chsh` (от слов change shell).

Пример команды:
```bash
$ sudo chsh -s /bin/bash UserName
```

Так же есть еще один способ. Необходимо отредактировать файл `/etc/passwd`
```bash
$ sudo nano /etc/passwd
```

В строке нашего пользователя сменить `/bin/sh` на `/bin/bash`
```bash
root:x:0:0:root:/root:/bin/bash
...
UserName:x:1000:1002::/home/UserName:/bin/bash
```

---
title: Установка GitLab Runner в Docker контейнер
description: GitLab Runner — это веб-приложение (агент), предназначенное для запуска и автоматического выполнения процессов CI/CD в GitLab. GitLab Runner выполняет задачи из файла .gitlab-ci.yml, который располагается в корневой директории вашего проекта.
slug: gitlab-runner
date: 2024-06-03
tags:
    - GitLab
    - CI/CD
    - Docker
    - DevOps
image: gitlab.jpg
---

## Переходим к установке и настройке

Первым делом необходимо установить `Docker`.
Docker желательно устанавливать всегда по официальной документации из [Docker.Docs][1]

> И не забываем после установки настроить [управление Docker от имени пользователя без полномочий root][2].

После установки `Docker` приступим к установке `GitLab-Runner` который будет работать в контейнере:

```Bash
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```
Как только образ загрузится и запустится контейнер переходим к регистрации Runner:

```Bash
docker exec -it gitlab-runner gitlab-runner register
```

Первым делом при регистрации нам нужно будет ввести адрес GitLab сервера (в моем случае это gitlab.com)

![Screenshot1](scr1.png)

Следующим шагом необходимо ввести authentication token который получить можно в настройках репозитория (Settings - CI/CD - Runers - New project runner)

Далее по порядку вводим:

* Enter a name for the runner: Вводим название ранера, или оставляем пустым
* Enter an executor: custom, ssh, parallels, virtualbox, docker-windows, docker+machine, docker-autoscaler, instance, shell, docker, kubernetes: вводим docker. Выбор способа выполнения вашего pipeline.
  * custom — как следует из названия — custom executor, нужен для работы со средами, которые gitlab не поддерживает «нативно», например Podman
  * ssh — не поддерживает сборочные кэши, выполняет сборки на удаленных машинах. Поддерживает только bash команды
  * parallels — работает аналогично virtualbox, только в качестве виртуализации использует parallels
  * virtualbox — создает из образа или снапшота виртуальную машину в vitrualbox, подключается к ней по ssh и выполняет действия указанные в .gitlab-ci.yml. По завершении машина удаляется
  * docker-windows
  * docker+machine — Предназначен больше для облачных решений и ЦОД. Создает виртуальные машины, устанавливает на них docker, и настраивает docker клиент для дальнейшей работы
  * docker-autoscaler
  * instance
  * shell — как из названия — выполняет все действия локально на машине, не подходит для использования gitlab-runner в docker контейнере. Бывает полезен в некоторых случаях, когда вам нужно внести изменения непосредственно в самой системе, или если у вас монолитное, не микросервисное, приложение.
  * docker — Подключается к docker-engine машины и выполняет все действия в изолированных контейнерах, которые по окончании уничтожаются (наиболее удобный вариант)
  * kubernetes — использует для сборок кластер kubernetes, подключаясь к kubernetes API
* Enter the default Docker image (for example, ruby:2.7): указываем используемый образ по умолчанию. Пишем docker:latest

В результате мы получим сообщении об успешной регистрации ранера

![Screenshot2](scr2.png)

## Для сборок docker-in-docker

Для сборки docker-in-docker, которую я очень рекомендую использовать, в связи с тем, что такой способ не оставляет кучи мусора на машине, рекомендую заново добавить `docker.sock` в runner volume. Иначе при сборке вы получите сообщение что docker не найден.

Необходимо отредактироват файл confit.toml:
```Bash
sudo nano /srv/gitlab-runner/config/config.toml
```
Приводим переменную `volume` к такому виду и сохраняем:
```Bash
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```

![Screenshot3](scr3.png)

Осталось только перезапустить контейнер и готово.
```Bash
docker restart gitlab-runner
```

[1]: https://docs.docker.com/engine/install/
[2]: https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user

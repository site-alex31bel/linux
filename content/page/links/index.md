---
title: Ссылки
links:
  - title: GitLab
    description: GitLab — это сервис для хранения кода, управления версиями и совместной разработки программного обеспечения.
    website: https://gitlab.com/alex31bel
    image: gitlab.png
  - title: Telegram
    description: Telegram — мессенджер для компьютера и смартфонов, позволяющий обмениваться сообщениями и медиафайлами.
    website: https://t.me/alex31bel
    image: telegram.png
menu:
    main:
        weight: -50
        params:
            icon: link

comments: false
---
